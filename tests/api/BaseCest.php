<?php

use App\Models\Library;

class BaseCest {

    protected $method = 'GET';
    protected $endpoint = '/';

    function _before(ApiTester $I) {
        $I->haveHttpHeader('Accept', 'application/json');
    }

    protected function getTestDescription($description) {
        return sprintf('Ensure api%s (%s) endpoint %s', $this->endpoint, $this->method, $description);
    }

    protected function wantTo($I, $message) {
        $I->wantTo(self::getTestDescription($message));
    }

}
