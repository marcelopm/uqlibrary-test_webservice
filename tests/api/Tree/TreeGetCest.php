<?php

/**
 * Test suite for findSmallestLeaf api endpoint:
 *
 * /api/findSmallestLeaf/{tree} GET
 *
 * Requirements:
 * - takes a parameter 'tree' JSON representation of an unsorted binary tree
 * - returns a minimum value of an unsorted binary tree leaf
 * - Leaf is a tree element which doesn't have any children.
 */
class TreeGetCest extends BaseCest {

    protected $endpoint = '/findSmallestLeaf';

    const SAMPLE_PAYLOAD = [
        'root' => 1,
        'left' => [
            'root' => 7,
            'left' => [
                'root' => 2,
            ],
            'right' => [
                'root' => 6,
            ],
        ],
        'right' => [
            'root' => 5,
            'left' => [
                'root' => 9,
            ],
        ],
    ];

    const SIMPLE_PAYLOAD = [
        'root' => 1,
        'left' => [
            'root' => 5,
            'left' => [
                'root' => 9,
            ],
        ],
    ];

    /**
     * @param ApiTester $I
     */
    public function samplePayloadResponseTest(ApiTester $I) {
        self::wantTo($I, 'returns 2 for the sample paylaod');
        $I->sendGET(sprintf('%s/%s', $this->endpoint, json_encode(self::SAMPLE_PAYLOAD)));
        $I->seeResponseCodeIs(\Codeception\Util\HttpCode::OK);
        $I->seeResponseEquals(2);
    }

    /**
     * @param ApiTester $I
     */
    public function simplePayloadResponseTest(ApiTester $I) {
        self::wantTo($I, 'returns 9 for the simple paylod');
        $I->sendGET(sprintf('%s/%s', $this->endpoint, json_encode(self::SIMPLE_PAYLOAD)));
        $I->seeResponseCodeIs(\Codeception\Util\HttpCode::OK);
        $I->seeResponseEquals(9);
    }

    /**
     * @param ApiTester $I
     */
    public function invalidTreeResponseTest(ApiTester $I) {
        self::wantTo($I, 'returns null for a malformed tree');
        $I->sendGET(sprintf('%s/%s', $this->endpoint, json_encode(['left' => 2])));
        $I->seeResponseCodeIs(\Codeception\Util\HttpCode::OK);
        $I->seeResponseEquals(null);
    }

    /**
     * @param ApiTester $I
     */
    public function emptyTreeTest(ApiTester $I) {
        self::wantTo($I, 'requires a JSON representation of a binary tree');
        $I->sendGET($this->endpoint);
        $I->seeResponseCodeIs(\Codeception\Util\HttpCode::NOT_FOUND);
    }

    /**
     * @param ApiTester $I
     */
    public function nonJsonInputTreeTest(ApiTester $I) {
        self::wantTo($I, 'requires JSON as a param');
        $I->sendGET(sprintf('%s/%s', $this->endpoint, str_random(rand(1, 20))));
        $I->seeResponseCodeIs(\Codeception\Util\HttpCode::OK);
        $I->seeResponseEquals(null);
    }

}
