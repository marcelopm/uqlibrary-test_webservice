<?php

/**
 * Test suite for library save info api endpoint:
 *
 * /api/library POST
 *
 * Requirements:
 * - takes a parameter 'library' JSON representation of a library object
 * - requires an authentication token X-VALID-USER: ${token}
 * - without auth token request should return unauthorised response
 * - token value can be any value for a successful response
 * - saves library data (data doesn't have to be saved (just demostrate Model layer), but you can save it into a file, DB, cache, cloud service etc, etc)
 */
use App\Models\Library;

class LibraryPostCest extends LibraryCest {

    protected $method = 'POST';

    function _before(ApiTester $I) {
        parent::_before($I);

        $I->haveHttpHeader('Content-Type', 'application/json');
    }

    protected function haveAuthToken($I) {
        $I->haveHttpHeader('X-VALID-USER', base64_encode(str_random(16)));
    }

    /**
     * From requirements: takes a parameter 'library' JSON representation of a library object
     *
     * @param ApiTester $I
     */
    public function invalidPayloadTest(ApiTester $I) {
        self::wantTo($I, 'requires a payload as input');
        self::haveAuthToken($I);
        $I->sendPOST($this->endpoint);
        $I->seeResponseCodeIs(\Codeception\Util\HttpCode::UNPROCESSABLE_ENTITY);
    }

    /**
     * From requirements: requires an authentication token X-VALID-USER: ${token}
     *
     * @param ApiTester $I
     */
    public function invalidAuthHeaderTest(ApiTester $I) {
        self::wantTo($I, 'requires a valid auth header');
        $I->sendPOST($this->endpoint, self::getFakePayload()->toJson());
        $I->seeResponseCodeIs(\Codeception\Util\HttpCode::UNAUTHORIZED);
    }

    /**
     * takes a parameter 'library' JSON representation of a library object
     * From requirements: saves library data (data doesn't have to be saved (just demostrate Model layer), but you can save it into a file, DB, cache, cloud service etc, etc)
     *
     * @param ApiTester $I
     */
    public function saveNewDataTest(ApiTester $I) {
        $payload = factory(Library::class)->make();

        self::wantTo($I, 'saves the payload information of a new library into storage');
        self::haveAuthToken($I);
        $I->sendPOST($this->endpoint, $payload->toJson());
        $I->seeResponseCodeIs(\Codeception\Util\HttpCode::OK);
        $I->seeRecord('App\Models\Library', $payload->toArray());
    }

    /**
     * takes a parameter 'library' JSON representation of a library object
     * From requirements: saves library data (data doesn't have to be saved (just demostrate Model layer), but you can save it into a file, DB, cache, cloud service etc, etc)
     *
     * @param ApiTester $I
     */
    public function updateDataTest(ApiTester $I) {
        $payload = self::getFakePayload();

        self::wantTo($I, 'c');
        self::haveAuthToken($I);
        $I->sendPOST($this->endpoint, $payload->toJson());
        $I->seeResponseCodeIs(\Codeception\Util\HttpCode::OK);
        $I->seeRecord('App\Models\Library', $payload->toArray());
    }

    /**
     * takes a parameter 'library' JSON representation of a library object
     * From requirements: saves library data (data doesn't have to be saved (just demostrate Model layer), but you can save it into a file, DB, cache, cloud service etc, etc)
     *
     * @param ApiTester $I
     */
    public function updateDataErrorTest(ApiTester $I) {
        $payload = self::getFakePayload();
        $payload->id = 100;

        self::wantTo($I, 'tries to save the payload information of a nonexistent library into storage');
        self::haveAuthToken($I);
        $I->sendPOST($this->endpoint, $payload->toJson());
        $I->seeResponseCodeIs(\Codeception\Util\HttpCode::NOT_FOUND);
    }

}
