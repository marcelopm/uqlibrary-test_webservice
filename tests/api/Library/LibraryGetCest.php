<?php

/**
 * Test suite for library info api endpoint:
 *
 * /api/library/{id} GET
 *
 * Requirements:
 * - takes 'id'
 * - 'id' is a number
 * - invalid id should return not found response
 * - returns a JSON with library details (returned data can be mocked, hardcoded, loaded from local file, DB, cache, cloud service, etc)
 */
class LibraryGetCest extends LibraryCest {

    /**
     * From requirements: takes 'id'
     *
     * @param ApiTester $I
     */
    public function emptyIdParamTest(ApiTester $I) {
        self::wantTo($I, 'requires an id as input');
        $I->sendGET($this->endpoint);
        $I->seeResponseCodeIs(\Codeception\Util\HttpCode::NOT_FOUND);
    }

    /**
     * From requirements: 'id' is a number
     *
     * @param ApiTester $I
     */
    public function nonNumericIdParamTest(ApiTester $I) {
        $ramdomStringId = str_random(6);

        self::wantTo($I, 'requires an integer id as an input');
        $I->sendGET(sprintf('%s/%s', $this->endpoint, $ramdomStringId));
        $I->seeResponseCodeIs(\Codeception\Util\HttpCode::NOT_FOUND);
    }

    /**
     * From requirements: returns a JSON with library details
     *
     * Validation rules:
     * - 'id' is a positive number
     * - 'code' is a 3 character, 3 number combination ARC101
     * - 'name' is a string
     * - 'abbr' is a string
     * - 'url' is a valid URL
     *
     * @param ApiTester $I
     */
    public function validResponseTest(ApiTester $I) {
        self::wantTo($I, 'generates a response containing specific key:value[type]');
        $I->sendGET(sprintf('%s/%d', $this->endpoint, self::getFakePayload()['id']));
        $I->seeResponseCodeIs(\Codeception\Util\HttpCode::OK);
        $I->seeResponseIsJson();
        $I->seeResponseMatchesJsonType([
            'id' => 'integer',
            'code' => 'string',
            'name' => 'string',
            'abbr' => 'string',
            'url' => 'string:url',
        ]);
    }

}
