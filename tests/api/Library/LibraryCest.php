<?php

use App\Models\Library;

class LibraryCest extends BaseCest {

    protected $method = 'GET';
    protected $endpoint = '/library';

    protected function getFakePayload() {
        return factory(Library::class)->states('sample')->make();
    }

}
