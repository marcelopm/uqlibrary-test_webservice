<?php

namespace App\Exceptions\Api;

use \Illuminate\Validation\ValidationException as BaseException;

/**
 * Custom validation exception class needed to make validations errors yeld exceptions
 *
 * Instead, it's possible to change the handler to report for the rest of the app.
 * But we want this specific to API call, @see \App\Http\Requests\Api\Request
 */
class ValidationException extends BaseException
{
}
