<?php

namespace App\Support;

use App\Contracts\DataStructure\BinaryTree;

/**
 * Binary search tree class helper
 */
class BSTHelper implements BinaryTree {

    /**
     * Return the smallest values for a given unsorted binary tree array
     * 
     * @param array $node
     * @return int
     */
    public function smallest($node) {

        // if doesn't have any child leaf, return its value or null
        if (!self::hasLeaf($node)) {
            return $node['root'] ?? null;
        }

        // in case there is a left leaf, get the smallest from there
        if (self::hasLeft($node)) {
            $left = self::smallest($node['left']);
        }

        // in case there is a right leaf, get the smallest from there
        if (self::hasRight($node)) {
            $right = self::smallest($node['right']);
        }

        $smallest = min($left ?? PHP_INT_MAX, $right ?? PHP_INT_MAX);

        // return the smallest form the smallest returned from the left and right leafs
        return $smallest !== PHP_INT_MAX ? $smallest : null;
    }

    /**
     * Check for leafs
     *
     * @param array $node
     * @return boolean
     */
    public function hasLeaf($node) {
        return self::hasLeft($node) || self::hasRight($node);
    }

    /**
     * Check for left leaf
     *
     * @param array $node
     * @return boolean
     */
    public function hasLeft($node) {
        return isset($node['left']);
    }

    /**
     * Check for right leaf
     *
     * @param array $node
     * @return boolean
     */
    public function hasRight($node) {
        return isset($node['right']);
    }

}
