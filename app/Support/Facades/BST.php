<?php

namespace App\Support\Facades;

use Illuminate\Support\Facades\Facade;

/**
 * @see \App\Support\BSTHelper
 */

class BST extends Facade
{
    /**
     * Get the registered name of the component.
     *
     * @return string
     */
    protected static function getFacadeAccessor()
    {
        return \App\Support\BSTHelper::class;
    }
}
