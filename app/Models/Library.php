<?php

namespace App\Models;

use Illuminate\Notifications\Notifiable;
use Illuminate\Database\Eloquent\Model;

class Library extends Model {

    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'code', 'name', 'abbr', 'url',
    ];

    /**
     * The attributes that shouldn't be exported - toArray, toJson
     */
    protected $hidden = [
        'created_at', 'updated_at'
    ];

}
