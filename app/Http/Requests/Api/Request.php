<?php

namespace App\Http\Requests\Api;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Contracts\Validation\Validator;
use App\Exceptions\Api\ValidationException;

class Request extends FormRequest {

    /**
     * Overridden so it's always authorized.
     *
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize() {
        return true;
    }

    /**
     * Overridden so errors are wrapped @see parent::failedValidation()
     * 
     * Format the errors from the given Validator instance.
     *
     * @param  \Illuminate\Contracts\Validation\Validator  $validator
     * @return array
     */
    protected function failedValidation(Validator $validator) {
        throw new ValidationException($validator, $this->response(
                ['error' => $this->formatErrors($validator)]
        ));
    }

    /**
     * Overridden so that generated responses are json formated, @see parent:isJson()
     *
     * Determine if the request is sending JSON.
     *
     * @return bool
     */
    public function isJson() {
        return true;
    }

    /**
     * Overridden so that generated responses are json formated, @see parent:expectsJson()
     *
     * Determine if the current request probably expects a JSON response.
     *
     * @return bool
     */
    public function expectsJson() {
        return true;
    }

    /**
     * Overridden so that generated responses are json formated, @see parent:wantsJson()
     *
     *  Determine if the current request is asking for JSON in return.
     *
     * @return bool
     */
    public function wantsJson() {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules() {
        return [];
    }

}
