<?php

namespace App\Http\Requests\Api\Library;

use Illuminate\Foundation\Http\FormRequest;
use App\Http\Requests\Api\Request;

class StoreRequest extends Request {

    /**
     * Overridden so it's based on the presence of the required custom header.
     *
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize() {
        // check for auth header
        return !empty(self::header('X-VALID-USER'));
    }

    /**
     * Overridden so it returns a 401 instead @see parent::forbiddenResponse()
     *
     * Get the response for a forbidden operation.
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function forbiddenResponse() {
        return response()->json([
            'error' => 'unauthorised'
        ], 401);
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules() {
        return [
            'id' => 'integer|min:0',
            'code' => 'required|string|max:255',
            'name' => 'required|string|max:255',
            'abbr' => 'required|string|max:255',
            'url' => 'required|url|max:255',
        ];
    }
}
