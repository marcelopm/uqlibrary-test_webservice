<?php

namespace App\Http\Controllers\Api;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use App\Models\Library;
use App\Http\Requests\Api\Library\StoreRequest;
use Symfony\Component\HttpKernel\Exception\HttpException;

class LibraryController extends BaseController {

    use DispatchesJobs,
        ValidatesRequests;

    /**
     * Stores a give library represented by an array of information
     *
     * @param StoreRequest $request
     * @return string
     */
    public function store(StoreRequest $request) {
        try {
            // get the relevant information from the request params
            $params = $request->only(['id', 'code', 'name', 'abbr', 'url']);

            // if id is not present
            if (empty($params['id'])) {
                // create a new model with them and store into storage
                $library = new Library($params);
                $library->save();
            } else {
                // otherwise, update an existing one
                $result = Library::where('id', $params['id'])
                    ->update($params);
                
                if (!$result) {
                    throw new HttpException(404, 'Record not found');
                }
            }

            return response()->json([
                'message' => 'Library stored'
            ]);
        } catch (HttpException $e) {

            // Render exception as JSON
            return response()->json([
                'error' => $e->getMessage()
            ], $e->getStatusCode());
        } catch (\Exception $e) {

            // Render exception as JSON
            return response()->json([
                'error' => $e->getMessage()
            ], 500);
        }
    }

}
