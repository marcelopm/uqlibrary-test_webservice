<?php

namespace App\Http\Controllers\Api;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use App\Http\Requests\Api\Request;
use App\Support\Facades\BST;

class TreeController extends BaseController {

    use DispatchesJobs,
        ValidatesRequests;

    /**
     * Print the smallest leave value for a give json string representation of an unsorted binary tree
     *
     * @param Request $request
     * @return string
     */
    public function findSmallestLeaf(Request $request) {
        /* Get the json string from the path segments since in-built request methods
         * seems unable to parse params outside the standard format
         */
        $json = $request->segment(3, '{}');

        // decode json into a array tree
        $tree = json_decode($json, true);

        // get the smallest using
        return response(BST::smallest($tree));
    }

}
