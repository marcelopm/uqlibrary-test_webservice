<?php

namespace App\Contracts\DataStructure;

/**
 * A contract with a few terms for a limit binary tree structure
 */
interface BinaryTree
{
    /**
     * Get the smallest node value
     *
     * @return int
     */
    public function smallest($node);

    /**
     * Check for leafs
     *
     * @return boolean
     */
    public function hasLeaf($node);

    /**
     * Check for left leafs
     *
     * @return boolean
     */
    public function hasLeft($node);

    /**
     * Check for right leafs
     *
     * @return boolean
     */
    public function hasRight($node);
}
