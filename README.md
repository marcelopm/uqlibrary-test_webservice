# UQ Library Backend test - PHP Web Service

This project was created as part of the requirement from the [UQ Library backend test](https://github.com/uqlibrary/work-test#backend-test-php-web-service)

The project provides a simple RESTful API, manly built above [Laravel] framework.

Once up and running, it exposes three diferent API endpoints:

* **/api/library/{id}** GET
    * returns a JSON with library details
* **/api/library** POST
    * saves the library submitted information
* **/api/findSmallestLeaf** GET
    * takes a parameter 'tree', which is a JSON representation of an unsorted binary tree, and returns a minimum value of an unsorted binary tree leaf

### `API`

For detailed information on the API, please see the [API Docs](API.md)

## Tech

The project is build above [Laravel] framework and used [Codeception] for tests.

## Installation

Checkout the master:

```sh
git clone https://bitbucket.org/marcelopm/uqlibrary-test_webservice {dir}
```

The project requires [npm] and [Composer], so make sure they are installed before trying to install the dependencies:

```sh
$ cd {dir}
$ composer install
```

From the [Laravel]'s doco:
> [..] you may need to configure some permissions. Directories within the  storage and the bootstrap/cache directories should be writable by your web server or Laravel will not run [..]

If you have any issues, this should do it for running the project locally:

```sh
$ sudo chmod -R 777 storage bootstrap/cache
```

### Configuration

Create a .env file based on the sample provided:

```sh
$ cp .env.example .env
```

Create Laravel's app key:

```sh
$ php artisan key:generate
```

##### Database

The project comes configured to use SQLite, please make sure the required PHP extension is installed and enabled. More information in [here](http://php.net/manual/en/sqlite.installation.php).

To use another RDBMS, please add and set the following keys to .env file:

```bash
DB_HOST=
DB_DATABASE=
DB_USERNAME=
DB_PASSWORD=
```
More information can be found [here](https://laravel.com/docs/5.3/database)

Otherwise create a SQLite database:

```bash
$ touch database/database.sqlite
```

And run the following command to create tables and populate them:

```sh
$ php artisan migrate --seed
```

This should create a library record on the libraries table, represented by the following JSON:

```JSON
{
  "id":   10123, 
  "code": "ARC100", 
  "name": "Architecture / Music Library", 
  "abbr": "Arch Music", 
  "url": "http://www.library.uq.edu.au/locations/architecture-music-library" 
}
```

##### Tests

The project tests use .env.testing file for configuraion. It comes with SQLite, in memory table type, configure for the sake of simplicity and performance. For another RDBMS, please referer to the instructions described above.  

### Running

Finally you should be able to start PHP's built-in dev server by running:

```sh
$ php artisan serve
```

And everything should be good to go with the API project running on [localhost]

A couple of the API endpoints can be accessed through:

- http://localhost:8000/api/library/10123
- http://localhost:8000/api/findSmallestLeaf/%7B%22root%22:1,%22left%22:%7B%22root%22:7,%22left%22:%7B%22root%22:2%7D,%22right%22:%7B%22root%22:6%7D%7D,%22right%22:%7B%22root%22:5,%22left%22:%7B%22root%22:9%7D%7D%7D

### Testing

The project uses [Codeception] for unit testing, which is included and installed from the dependency list out of the box.

To run the tests, just use the command bellow:

```sh
$ php vendor/bin/codecept run
```

**In case of previously compiled classes error**

```bash
PHP Fatal error:  Cannot declare class Symfony\Component\Finder\SplFileInfo, because the name is already in use in {din}/bootstrap/cache/compiled.php
...
```

Bug described at: [https://github.com/Codeception/Codeception/issues/3393](https://github.com/Codeception/Codeception/issues/3393)

Please run:

```bash
$ php artisan clear

The compiled class file has been removed.
```

And retry

```sh
$ php vendor/bin/codecept run
```

Notes:

The app checks for a custom auth header when request are made to api/library POST endpoint.

This is part of the test requirements.

However, this check is done in a very basic way: 

```php
app/Http/Requests/Api/Library/StoreRequest.php

use App\Http\Requests\Api\Request;

class StoreRequest extends Request {
...
    public function authorize() {
        // check for auth header
        return !empty(self::header('X-VALID-USER'));
    }
```

In a real world scenario, ideally, this would be posible be done using a custom guard. More information on how this can be achieved in here: [https://laravel.com/docs/5.3/authentication#adding-custom-guards](https://laravel.com/docs/5.3/authentication#adding-custom-guards)

License
----

[![Creative Commons Licence](https://i.creativecommons.org/l/by-nc-sa/4.0/88x31.png)](http://creativecommons.org/licenses/by-nc-sa/4.0/)  
UQ Library Backend Test - PHP Web service by [Marcelo Moises](https://bitbucket.org/marcelopm/uqlibrary-test_webservice/) is licensed under a [Creative Commons Attribution-NonCommercial-ShareAlike 4.0 International License](http://creativecommons.org/licenses/by-nc-sa/4.0/).  
Based on a work at [https://bitbucket.org/marcelopm/uqlibrary-test_webservice/](https://bitbucket.org/marcelopm/uqlibrary-test_webservice/).

[//]: # (These are reference links used in the body of this note and get stripped out when the markdown processor does its job. There is no need to format nicely because it shouldn't be seen. Thanks SO - http://stackoverflow.com/questions/4823468/store-comments-in-markdown-syntax)

[localhost]: <http://localhost:8000>
[laravel]: <https://github.com/laravel/laravel>
[composer]: <https://github.com/composer/composer>
[npm]: <https://github.com/npm/npm>
[codeception]: <https://github.com/codeception/codeception>