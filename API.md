## REST API:

## Library

### Resource URI

    api/library/{id} GET

### Parameters

| Parameter  | Required | Default | Description
| ---------  | -------- | ------- | -----------
| id         | true     |         | Institution id

### Resource Properties

| Property      | Description
| ------------- | -----------
| id            | Library id.
| code          | Library additional notes.
| name          | Library name.
| abbr          | Library abbreviation
| url           | Library url.

### HTTP GET

Returns an object of library.

Example usage:

```
curl \
  -H "Content-Type: application/json" \
  http://localhost:8000/api/library/10123
```

If the request is successful a HTTP 200 response is returned with the following response body
```json
{
  "id":   10123, 
  "code": "ARC100", 
  "name": "Architecture / Music Library", 
  "abbr": "Arch Music", 
  "url": "http://www.library.uq.edu.au/locations/architecture-music-library" 
}
```

### Resource URI

    api/library POST

### Parameters

For new records:

| Parameter  | Required | Default | Description
| ---------  | -------- | ------- | -----------
| code       | True     |         | Institution additional notes
| name       | True     |         | Institution name
| abbr       | True     |         | Institution abbreviation
| url        | True     |         | Institution url

For existing records:

| Parameter  | Required | Default | Description
| ---------  | -------- | ------- | -----------
| id         | true     |         | Institution id
| code       | false    |         | Institution additional notes
| name       | false    |         | Institution name
| abbr       | false    |         | Institution abbreviation
| url        | false    |         | Institution url

Auth header:

| Parameter    | Required | Default | Description
| -------------| -------- | ------- | -----------
| X-VALID-USER | true     |         | Auth Header mock string

### Resource Properties

| Property      | Description
| ------------- | -----------
| message       | response message
| error         | response error

### HTTP POST

Save an object of library into storage

Example usage:

```
curl \
-X POST \
-H "X-VALID-USER: qwe" \
-d '{"code":"ASK100","name":"AskUs chat & phone assistance","abbr":"AskUs","url":"https://web.library.uq.edu.au/contact-us"}' \
"http://localhost:8000/api/library"
```

If the request is successful a HTTP 200 response is returned with the following response body
```json
{
  "message": "Library stored"
}
```

## Binary Tree

### Resource URI

    api/findSmallestLeaf/{tree} GET

### Parameters

| Parameter  | Required | Default | Description
| ---------  | -------- | ------- | -----------
| tree       | true     |         | JSON representation of an unsorted binary tree

### Resource Properties

smallest leaf represent by a number

### HTTP GET

Returns an object of library.

Example usage:

```
curl -X GET \
"http://localhost:8000/api/findSmallestLeaf/%7B%22root%22:1%2C%22left%22:%7B%22root%22:7%2C%22left%22:%7B%22root%22:2%7D%2C%22right%22:%7B%22root%22:6%7D%7D%2C%22right%22:%7B%22root%22:5%2C%22left%22:%7B%22root%22:9%7D%7D%7D"
```

If the request is successful a HTTP 200 response is returned with the following response body
```text
2
```