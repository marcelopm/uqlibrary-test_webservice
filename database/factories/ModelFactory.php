<?php

/*
  |--------------------------------------------------------------------------
  | Model Factories
  |--------------------------------------------------------------------------
  |
  | Here you may define all of your model factories. Model factories give
  | you a convenient way to create models for testing and seeding your
  | database. Just tell the factory how a default model should look.
  |
 */

/** @var \Illuminate\Database\Eloquent\Factory $factory */
$factory->define(App\User::class, function (Faker\Generator $faker) {
    static $password;

    return [
        'name' => $faker->name,
        'email' => $faker->unique()->safeEmail,
        'password' => $password ?: $password = bcrypt('secret'),
        'remember_token' => str_random(10),
    ];
});

// Define a random library factory
$factory->define(App\Models\Library::class, function (Faker\Generator $faker) {
    $name = $faker->sentence(2);

    return [
        'code' => strtoupper($faker->unique()->bothify('???###')),
        'name' => ucwords($name),
        'abbr' => strtoupper(preg_replace('~\b(\w)|.~', '$1', $name)),
        'url' => $faker->url,
    ];
});

// Define a sample library factory
$factory->state(App\Models\Library::class, 'sample', function (Faker\Generator $faker) {
    // based on Sample JSON structure
    return [
        'id' => 10123,
        'code' => 'ARC100',
        'name' => 'Architecture / Music Library',
        'abbr' => 'Arch Music',
        'url' => 'http://www.library.uq.edu.au/locations/architecture-music-library',
    ];
});
