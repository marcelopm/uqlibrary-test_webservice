<?php

use Illuminate\Database\Seeder;
use App\Models\Library;

class LibrariesTableSeeder extends Seeder {

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run() {
        // get the sample library from the ModelFactory
        $sample = factory(Library::class)->states('sample')->make();

        // and store to the database in case it doesn't already exist
        if (Library::where('code', $sample->code)->count() < 1) {
            $sample->save();
        }
    }

}
