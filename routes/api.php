<?php

use Illuminate\Http\Request;

/*
  |--------------------------------------------------------------------------
  | API Routes
  |--------------------------------------------------------------------------
  |
  | Here is where you can register API routes for your application. These
  | routes are loaded by the RouteServiceProvider within a group which
  | is assigned the "api" middleware group. Enjoy building your API!
  |
 */

Route::get('/user', function (Request $request) {
    return $request->user();
})->middleware('auth:api');

/**
 * Library GET routes
 * 
 * Requirements:
 * - takes 'id'
 * - 'id' is a number
 * - invalid id should return not found response
 * - returns a JSON with library details (returned data can be mocked, hardcoded, loaded from local file, DB, cache, cloud service, etc)
 */
Route::group(['name' => 'getLibrary'], function () {
    // 'id' is a number
    Route::get('/library/{library}', function (App\Models\Library $library) {
        // returns a JSON with library details
        return response()->json($library);
    })->where('library', '[0-9]+');

    // From requirements: takes 'id', invalid id should return not found response
    Route::get('/library', function (Request $request) {
        return response()->json(['error' => 'Not found'], 404);
    });

    // From requirements: 'id' is a number, invalid id should return not found response
    Route::get('/library/{invalidId}', function (Request $request) {
        return response()->json(['error' => 'Not found'], 404);
    })->where('invalidId', '.+');
});

/**
 * Library POST route
 *
 * Requirements:
 * - takes a parameter 'library' JSON representation of a library object
 * - requires an authentication token X-VALID-USER: ${token}
 * - without auth token request should return unauthorised response
 * - token value can be any value for a successful response
 * - saves library data (data doesn't have to be saved (just demostrate Model layer), but you can save it into a file, DB, cache, cloud service etc, etc)
 */
Route::post('/library', 'Api\LibraryController@store');

/**
 * /api/findSmallestLeaf GET routes
 *
 * Requirements:
 * - takes a parameter 'tree' JSON representation of an unsorted binary tree
 * - returns a minimum value of an unsorted binary tree leaf
 * - Leaf is a tree element which doesn't have any children.
 */
Route::get('/findSmallestLeaf/{tree}', 'Api\TreeController@findSmallestLeaf');

// From requirements: takes a parameter 'tree' JSON representation of an unsorted binary tree
Route::get('/findSmallestLeaf', function (Request $request) {
    return response()->json(['error' => 'Not found'], 404);
});
